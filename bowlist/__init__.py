#!/usr/bin/python3

from flask import Flask, render_template, request,\
    redirect, url_for, abort, session, make_response

from flask_script import Manager, Shell, Server

from flask_sqlalchemy import SQLAlchemy

from sqlalchemy.orm import class_mapper, ColumnProperty

from datetime import datetime, timedelta
from os import environ as env

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = env.setdefault('BOWLIST_DB_URI', 'sqlite:///bowlist.sqlite3')
app.config['SQLALCHEMY_ECHO'] = True
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
app.config['DEBUG']  = True
app.config['EXPIRE'] = 900
app.config['TRIM']   = 3600
app.config['LAST_NEW_STATE'] = 'announced'


db = SQLAlchemy(app)

manager = Manager(app)
manager.add_command("server", Server())
manager.add_command("shell", Shell())

# These are the states a reservation cycles through
state_map = { 'announced': "Înscris",
              'arrived':   "Prezent",
              'cancelled': "Renunțat",
              'expired':   "Expirat",
              'served':    "Servit" }



class Entry(db.Model):
    id          = db.Column(db.Integer, primary_key=True)
    name        = db.Column(db.String)
    info        = db.Column(db.String)
    create_time = db.Column(db.DateTime)
    play_time   = db.Column(db.DateTime)
    modify_time = db.Column(db.DateTime)
    state       = db.Column(db.Enum(*[i for i in state_map]))

    def __init__(self, name, play_time=None, info="", state='announced'):
        self.name = name
        self.info = info
        self.create_time = datetime.now()
        self.play_time = play_time or datetime.now()
        self.modify_time = datetime.now()
        self.state = state if len(state)>0 else 'announced'

@manager.command
def expiredb(now=None, deadline=600):
    '''
    Cancels overdue entries that are announced.
    '''
    if now is None:
        now = datetime.now()

    # expire non-present entries
    expiry_deadline = now-timedelta(seconds=deadline)
    Entry.query.\
        filter(Entry.state == 'announced',
               Entry.play_time <= expiry_deadline).\
        update({Entry.state: 'expired',
                Entry.modify_time: now})

    db.session.commit()


@manager.command
def trimdb(now=None, deadline=3600):
    '''
    Deletes old entries that are cancelled, served or expired
    '''
    if now is None:
        now = datetime.now()

    remove_deadline = now-timedelta(seconds=deadline)
    Entry.query.\
        filter(Entry.state.in_(['expired', 'cancelled', 'served']),
               Entry.modify_time <= remove_deadline).\
        delete(synchronize_session=False)

    db.session.commit()
    

@manager.command
def createdb():
    print ("Database URI:", env['BOWLIST_DB_URI'])
    db.create_all()


@app.route('/expire_and_trim')
def expire_and_trim():
    if app.config['EXPIRE'] >= 0:
        expiredb(deadline=app.config['EXPIRE'])
    if app.config['TRIM'] >= 0:
        trimdb(deadline=app.config['TRIM'])

        
def check_expire_trim():
    '''
    Checks the expire/trim parameters and saves them into the config file.
    '''
    exp = request.args.get('expire', None)
    if exp is not None:
        try:
            app.config['EXPIRE'] = int(exp)
        except: pass
        
    trm = request.args.get('trim', None)
    if trm is not None:
        try:
            app.config['TRIM']   = int(trm)
        except: pass
    
    

@app.route('/',      methods=["GET"])
@app.route('/index', methods=["GET"])
@app.route('/admin', methods=["GET"])
def index():
    '''
    Outpus a standard waiting list.
    If URL is /admin, then admin.html is rendered (which
    gives editorial control over the waiting lists).
    '''

    
    output = 'index.html'
    if request.path == '/admin':
        output = 'admin.html'
        check_expire_trim()
        expire_and_trim()

    entries = Entry.query.order_by('create_time').all()
    
    return render_template(output, entries=entries,
                           now=datetime.now().strftime("%H:%M %Y-%m-%d"),
                           state_map=state_map,
                           refresh=int(request.args.get('refresh', '300')),
                           expire=app.config['EXPIRE'],
                           trim=app.config['TRIM'],
                           last_new_state=app.config['LAST_NEW_STATE'])


def parse_time(data):
    '''
    Returns a datetime object based on a string "HH:MM [YYYY-MM-DD]" format.
    '''
    try:
        return datetime.strptime(request.form['play_time'], "%H:%M %Y-%m-%d")
    except ValueError:
        usr_time = datetime.strptime(request.form['play_time'], "%H:%M")
        return datetime.combine(datetime.now().date(), usr_time.time())


@app.route('/entry',                           methods=['POST']) # creates new entry
@app.route('/entry/<int:id>',                  methods=['POST']) # updates all fields
@app.route('/entry/<int:id>/<string:field>',   methods=['POST']) # updates 'field', data from form
@app.route('/entry/<int:id>/<string:field>/<string:value>',
           methods=['POST', 'GET']) # updates specific field
def entry_set(id=None, field=None, value=None):
    '''
    Adds or updates entries
    '''
    
    if (id is None):
        ''' Create new object '''
        pt = parse_time(request.form['play_time'])
        entry = Entry(request.form['name'],
                      play_time=pt,
                      info=request.form['info'],
                      state=request.form['state'])
        app.config['LAST_NEW_STATE'] = request.form['state']
        db.session.add(entry)
        db.session.commit()
        
    elif field is None:
        ''' Update whole object from form data'''
        entry = Entry.query.filter_by(id=id).one()
        entry.name      = request.form['name']
        entry.info      = request.form['info']
        entry.play_time = parse_time(request.form['play_time'])
        if len(request.form['state']) > 0:
            entry.state       = request.form['state']
            entry.modify_time = datetime.now()
        db.session.commit()
        
    else:
        '''
        Update only specific field, form or url-encoded
        First, retrieve the corresponding entry.
        Then parse the data if it's a time stamp or an integer
        '''
        entry = Entry.query.filter_by(id=id).one()
        
        if value is None:
            value = request.form[field]
        if field == 'play_time' or field == 'create_time':
            value = parse_time(value)
        
        # filter out the correct field from 'entry', but make sure it's
        # a valid column first (don't want to return other data)
        for prop in class_mapper(Entry).iterate_properties:
            if isinstance(prop, ColumnProperty) and (field == prop.key):
                setattr(entry, field, value)
                if (field == 'state'):
                    entry.modify_time = datetime.now()
                db.session.commit()
                return make_response(str(getattr(entry, field)), 200)
    
    return redirect(url_for('index',
                            refresh=int(request.args.get('refresh', '0')) ) )


@app.route('/entry',                  methods=['GET'])
@app.route('/entry/<int:id>',         methods=['GET'])
@app.route('/entry/<int:id>/<field>', methods=['GET'])
def entry_get(id=None, field=None):
    entry = Entry.query.filter_by(id=id).one() if id is not None else Entry('')
    if field is None:
        return render_template('entry.html', entry=entry,
                               state_map=state_map, refresh=0)
    else:
        # filter out the correct field from 'entry', but make sure it's
        # a valid column first (don't want to return other data)
        for prop in class_mapper(Entry).iterate_properties:
            if isinstance(prop, ColumnProperty) and (field == prop.key):
                return make_response(str(getattr(entry, field)), 200)
            
        # invalid field
        return make_response ('', 403)

    
if __name__ == '__main__':
    db.create_all()
    manager.run()
