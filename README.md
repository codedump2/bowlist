# Bowlist

Bowlist is a simple web-based application to create scheduling and waiting
lists for a bowling alley management system. The basic idea is not
to allocate fixed time slots, but to place each request in a scheduling
queue -- first call, first served. The evaluation rules for who's up
next are as follows:

 - Calling ahead or presenting oneself at the counter leads to
   registry on the list at time `T-reg`.

 - The custmer communicates a desired time to be served with a lane,
   `T-play`.

 - The customer will be offered a bowling lane at the earliest possible
   time after `T-play`; if more than one customer is in the waiting queue,
   the order in which they'll be served is: oldest `T-reg` first.

 - If the customer is not available (i.e. not present) or declines to
   accept the offer, he's removed from the list.

 - The customer can, however, choose to yield their place, e.g. in person
   or by calling in advance. In this case they won't be removed from the list,
   but will be offered another lane at a later point.

## Installing

### Dependencies

 - python3-flask
 - python3-flask-sqlalchemy
 - python3-flask-script


## Running

### Directly

The Bowlist server can be started by directly calling the `bowlist.py` script in
the root of the project. Called with the `-?` argument, it presents itself
with a number of options:

```
    usage: bowlist [-?] {server,shell,expiredb,trimdb,createdb,runserver} ...

    positional arguments:
      {server,shell,expiredb,trimdb,createdb,runserver}
        server              Runs the Flask development server i.e. app.run()
        shell               Runs a Python shell inside Flask application context.
        expiredb            Cancels overdue entries that are announced.
        trimdb              Deletes old entries that are cancelled, served or expired
        createdb
        runserver           Runs the Flask development server i.e. app.run()

    optional arguments:
      -?, --help            show this help message and exit
```

Note that the Flask framework emphasizes that directly running a Flask
appliaction using the built-in HTTP server is only for development.
For production setups, use the [WSGI method](#running-bowlist-via-wsgi)
described below.

### Running Bowlist via WSGI

The folder `./apache` at the root of the Bowlist project contains files
necessary to run Bowlist via an Apache 2 HTTP server using the WSGI
gateway. For this, the following packages need to be installed:

 - `apache2`
 
 - `libapache2-mod-wsgi-py3`
 


Variables

 - `BOWLIST_PATH` The path at which the bowlist appliaction is located
 
 - `BOWLIST_KEY` The secret appliaction key
