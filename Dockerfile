FROM debian:11
RUN apt-get update && apt-get -y --no-install-recommends install \
	python3-flask \
	python3-flask-script \
	python3-flask-sqlalchemy \
	apache2 \
	libapache2-mod-wsgi-py3 \
	&& \
	rm -rf /etc/apache2/sites-enabled/000-default.conf && \
	sed -ri 's!^(TransferLog)\s+\S+!\1 /dev/stdout!g' /etc/apache2/apache2.conf && \
	sed -ri 's!^(ErrorLog)\s+\S+!\1 /dev/stderr!g' /etc/apache2/apache2.conf \
	&& \
	apt-get autoremove && apt-get autoclean && \
	useradd bowlist && \
	mkdir /home/bowlist && \
	mkdir -p /var/www/bowlist /var/lib/bowlist /home/bowlist && \
	chown bowlist.bowlist /var/www/bowlist /var/lib/bowlist /home/bowlist

COPY bowlist apache/bowlist.wsgi /var/www/bowlist

COPY apache/bowlist.conf /etc/apache2/sites-enabled

ENV BOWLIST_PATH=/var/www/bowlist \
    BOWLIST_DB_URI=sqlite:////var/lib/bowlist/bowlist.sqlite3 \
    APACHE_RUN_USER=bowlist \
    APACHE_RUN_GROUP=bowlist

# setting up bowlist
RUN	/var/www/bowlist/__init__.py createdb && \
	chown bowlist.bowlist /var/lib/bowlist -R

EXPOSE 80/tcp

CMD apache2ctl -DFOREGROUND
