#!/usr/bin/python

import sys, logging, os, random, string

logging.basicConfig (stream=sys.stderr)
sys.path.insert (0, os.path.join(os.environ['BOWLIST_PATH'], '..'))

from bowlist import app as application
application.secret_key = os.environ.setdefault(
		       'BOWLIST_KEY',
		       ''.join(random.choice(string.ascii_letters) for l in range(20)))

log = logging.getLogger(__name__)
log.info('WSGI key:', application.secret_key)